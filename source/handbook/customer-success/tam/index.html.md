---
layout: markdown_page
title: "Technical Account Management"
---
# Technical Account Management Handbook
{:.no_toc}

The Technical Account Team is part of [Customer Success](/handbook/customer-success).  The goal of the team is to deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments (Strategic, Large, Mid-Market) so that customers see the value in their GitLab investment, and we retain and drive growth in our customer base.

## On this page
{:.no_toc}

- TOC
{:toc}

## Role
Providing guidance, planning and oversight while leveraging adoption and technical best practices. The Technical Account Manager is the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab. Additionally, the TAM serves as the liaison between the customer and the GitLab ecosystem, streamlining collaboration with Product Management, Engineering, Sales, Professional Services and others.

## Responsibilities
* Contact new customer within 1 day of purchase
* Own, manage, and deliver consistent onboarding experience based on tier 
* Achieve onboarding milestones
* Send satisfaction survey 90 days post sale
* Actively build and maintain trusted and influential relationships
* Deliver consistent engagement model by tier
* Serve as a main POC
* Increase integrations, identify additional champions, drive utilization
* Help customer realize the value of investment
* Actively seek expansion opportunities. Work with Strategic Account Leader on developing and owning an [Account Plan](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24)
* Identify all impediments

## Tier Levels of Engagement

### Tier 1
*  $500,000 ARR
*  Accounts Per TAM: 5


### Tier 2
*  $250,000 ARR
*  Accounts Per TAM: 10



### Tier 3
*  $100,000 ARR
*  Accounts Per TAM: 25

# Engagement Models Defined

![image](images/handbook/sales/tam-table.png)
