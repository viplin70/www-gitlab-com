---
layout: markdown_page
title: "Accounting and Finance Procedures"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Accounting and Finance Procedures

The pages below contain step-by-step instructions on how to execute various accounting and Finance team responsibilities. The link to our month-end close checklist can also be found below.

- [Accounts Payable, Employee Expenses, and Other Liabilities](/handbook/finance/Accounts-payable-employee-expenses-and-other-liabilities/)
- [Accounts Receivable and Cash](/handbook/finance/Accounts-receivable-and-cash/)
- [Financial Planning Process](/handbook/finance/financial-planning-process/)
- [Processing Payroll](/handbook/finance/processing-payroll/)
- [Intercompany Settlement](/handbook/finance/intercompany-settlement/)
- [GitLab.com Cost Allocation](/handbook/finance/GitLab-com-cost-allocation/)
- [Swag Shop Transactions](/handbook/finance/Swag-shop-transactions/)
- [Asset Tracking](/handbook/finance/Asset-tracking/)
- [Month-End Close Checklist](https://docs.google.com/a/gitlab.com/spreadsheets/d/1SSUQpudxxpPgXIS97Ctuj-JRII0qhq0I3r19jmBKU7c/edit?usp=sharing)
