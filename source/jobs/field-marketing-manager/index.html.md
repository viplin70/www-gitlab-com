---
layout: job_page
title: "Field Marketing Manager"
---

GitLab is looking for a highly motivated, sales-focused field marketer to support our Sales Teams. The Field Marketing Manager position is responsible for all regional marketing supporting sales in their specific region.

The Field Marketing Manager - East is responsible for supporting our US East Coast and Federal Sales Teams.

The Field Marketing Manager - EMEA is responsible for supporting our Europe, Middle East, and Africa (EMEA) sales team.

A successful Field Marketing Manager has strong understanding of sales-focused marketing as well as our audiences of enterprise IT leaders, IT ops practitioners, and developers. They enjoy taking charge of regional marketing programs, detailed planning, proactive communication, and flawlessly delivering memorable marketing experiences that support our sales objectives.

## Responsibilities

* Adapt digital and content marketing programs to the needs of a regional sales team.
* Create, expand, and accelerate sales opportunities through regional and account-based marketing execution, within product-marketing defined strategy.
* Be an advocate for the sales region you support and help the rest of the marketing department understand their priorities.
* Be an advocate for the marketing department and help the sales team you support understand the marketing department's priorities.
* Swag management for sponsored events, GitLab owned events, and in support of the sales department.
* Vendor management for swag and regional marketing.
* Decision making and discretion regarding event selection and planning in support of regional sales objectives.
* Regional event strategy, decision making, and onsite management.
* Event logistics in support of the team. From helping to book space for meetings to making sure the booth is staffed, and making sure every aspect of our events are well organized.

## Requirements
- Past experience delivering, accelerating, and expanding sales pipeline through regional marketing.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders.
- Ability to empathize with the needs and experiences of IT leaders, IT ops practicioners, and developers.
- Extremely detail-oriented and organized, and able to meet deadlines.
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.
- A passion and strong understanding of the developer tools, IT operations tools, and/or IT security markets.
- Strong understanding of marketing to financial services companies and federal agencies.
- Experience with supporting both direct sales and channel sales teams.

## Additional Requirements for EMEA
- Past experience running marketing to develop DACH, BENELUX, Nordics, and/or UK & Ireland.
- Strong understanding of marketing to the automotive, banking, and tech sectors.

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Candidates will complete a take-home exercise crafting an account based marketing strategy from a provided prompt. Send the completed exercise to our Senior Director of Marketing and Sales Development.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Field Marketing Manager and Content Marketing Manager.
- Depending on location, candidates may then meet with either a Senior Product Marketing Manager or Sales Development Representative - EMEA.
- Candidates will then be asked to schedule 45 minute interviews with Regional Sales Directors of their respective regions, with the Senior Director of Marketing and Sales Development, and with our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
