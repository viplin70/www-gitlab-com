---
layout: job_page
title: "Accounting Manager"
---

{: .text-center}
<br>

GitLab is adding the next essential member who can help drive operational improvements within our finance department. You will join our team in its early stages responsible for developing a highly efficient, world class accounting process. We expect you will know your way around GAAP principles, financial statements, and be a proven problem solver.

## Responsibilities

* Supervise accounting staff on daily tasks
* Provide training to new and existing staff as needed.
* Ensure an accurate and timely month end close by providing leadership and support to the accounting close process. This role includes serving as a hands-on team member responsible for performing and reviewing transactions.
* Assist with daily banking requirements.  Monitor and forecast cash position
* Establish and enforce proper accounting policies and principles
* Assist in development and implementation of new procedures to enhance the workflow of the department.
* Responds to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Assist in preparing for and managing annual external audit.  Coordinate with other auditors as needed.
* Support overall department goals and objectives
* This person will initially coordinate third party providers as an individual contributor. Over time this person will be expected to build and manage a team.


## Requirements

* Proven work experience as an Accounting Manager or similar leadership role.  
* Ability to contribute to the career development of staff and a culture of teamwork
* Strong working knowledge of GAAP principles and financial statements
* Must have experience with Netsuite
* Proficient with excel and google sheets
* International experience preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* Proficiency with GitLab
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-operations/#background-checks).

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

