---
layout: job_page
title: "Director of Product Marketing"
---

As the Director of Product Marketing, you will be responsible for managing and building the team that focuses on GitLab's product, partner, and content marketing.

## Responsibilities

* Provide input about product priorities and goals to Product Management.
* Use customer and competitive research to influence product strategy and drive effective messaging.
* Develop and own product positioning, messaging and value propositions that differentiate GitLab solutions in the marketplace.
* Develop and own cross-team marketing campaigns and GTM process to ensure successful, integrated launches for new product features and solutions.
* Create a customer reference program to engage our customers and to showcase customer success and value realization achieved using GitLab.
* Craft engaging audience-specific marketing assets and content, including case studies, solution briefs, web content, presentations, data sheets, whitepapers and sales playbooks that can be utilized globally.
* Define market segmentation and develop the right marketing strategy that maps to target buyer, business problems, and IT/Engineering initiatives aligned with the business opportunity.
* Develop content and sales tools to accelerate conversion rates at each stage of the sales funnel.
* Partner with the Marketing & Sales Development team to build and execute campaigns.
* Define and execute sales enablement strategy for sales/business development, field, solution architect and partner-facing roles; developing and delivering sales enablement content and playbooks.
* Conduct ROI/TCO analysis and develop content to help customers make the business case for GitLab.
* Gather competitive intelligence on our key competitors and develop differentiation materials for the field and for external consumption.
* Develop and implement analyst engagement strategy and support industry analyst briefings.
* Shape and execute GitLab's PR strategy in collaboration with PR agency.
* Attract talented PMM professionals -  inspire and lead a happy and effective team through setting goals, rewarding success, and opening gateways for career growth.
* Support the team in setting clear objectives and key results. Ensure that those are communicated and visible across the company and progress is shared regularly.

## Requirements

* 8-10 years of experience in product marketing in the software industry, preferably within an area of application development.
* Technical background or clear understanding of developer products; familiarity with Git, Continuous Integration, Containers, Kubernetes and Project Management software a plus.
* Experience with Software-as-a-Service offerings a plus.
* Bachelor’s degree in computer science, marketing, business, or related area; MBA is a plus.
* Proven track record in building, getting buy-in and executing marketing plans, and staying focused on “getting it done” in a fast-moving, technical environment.
* Able to coordinate across many teams and perform in fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Demonstrated experience building credibility with and delivering results for product teams, sales teams and customers.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Uses data to measure results and inform decision making and strategy development.
* Excellent spoken and written English
* You share our [values](/handbook/values), and work in accordance with those values.

##Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Chief Marketing Officer
* A 45 minute interview with our Senior Director of Marketing and Sales Development
* A 45 minute interview with our East Coast Regional Sales Director
* A 45 minute interview with our Vice President of Product
* A 45 minute interview with our Chief Revenue Officer
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).


## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
